resource "google_storage_bucket" "av_terraform_bucket" {
  name     = var.bucket_name
  location = var.region_name

  # чтоб не грохнуть при дестрое
  lifecycle {
    prevent_destroy = true
  }

  # включаем версионирование
  versioning {
    enabled = true
  }
  force_destroy = false
}