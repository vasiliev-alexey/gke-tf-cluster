variable "project_name" {
  type = string
}

variable "cred_file" {
  type = string
}

variable "cluster_name" {
  default = "av-k8s-cluster"
}

variable "region_name" {
  default = "europe-west1"
}

variable "location_name" {
  default = "europe-west1-b"
}

variable "count_vms" {
  default = "1"
}
variable "dns_zone_name" {
  default = "example-com"
}


variable "machine_type" {
  default = "e2-standard-2"
}


variable "bucket_name" {
  description = "GCS Bucket name. Value should be unique ."
  type        = string
  default     = "ab-k8s-tf-bucket"
}
